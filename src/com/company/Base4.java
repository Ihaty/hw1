package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Base4 {
    Scanner in = new Scanner(System.in);

    public void start() {
        System.out.println("Функции");
        task1();
//        task2();
    }

    //Print the number in letters.
    public void task1() {
        String[] level1 = {"", "один", "два", "три", "четыре", "пять", "шесть", "семь", "восемь", "девять", "десять", "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать"};
        String[] level2 = {"", "", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто"};
        String[] level3 = {"", "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот"};
        String[] level4 = {"десять", "одинадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестьнадцать", "семьнадцать", "восемьнадцать", "девятьнадцать"};
        int num = in.nextInt(); //Ввод числа
        String strNum = Integer.toString(num); //Преобразование числа в строку
        char[] result = strNum.toCharArray(); //Преобразование строки в массив
        int getNum0Index = result[0] - '0';
        int getNum1Index = 0;
        int getNum2Index = 0;
        if (result.length != 1) {
            getNum1Index = result[1] - '0';
        }
        if (result.length == 3) {
            getNum2Index = result[2] - '0';
        }
        if (num == 0) {
            System.out.print("ноль");
        }
        String words;
        String words2;
        String words3;

        if (num < 20) {
            words = level1[num];
            System.out.println(words);
        } else if (num >= 20 && num <= 99) {
            //1 число к level2
            words = level2[getNum0Index];
            words2 = level1[getNum1Index];
            System.out.print(words + " ");
            System.out.println(words2);
        } else {
            words = level1[getNum2Index];
            words2 = level2[getNum1Index];
            words3 = level3[getNum0Index];
            System.out.print(words3 + " ");
            if (getNum1Index == 1) {
                System.out.println(level4[getNum2Index]);
            } else {
                System.out.print(words2 + " ");
                System.out.println(words);
            }
        }
    }

    //Print the letters in number.
    public void task2() {

    }
}
