package com.company;


import java.lang.String;
import java.util.Arrays;
import java.util.Scanner;

public class Base3 {
    Scanner in = new Scanner(System.in);

    public void start() {
        System.out.println("Одномерные массивы");
        task1();
        task2();
        task3();
        task4();
        task5();
        task6();
        task7();
        task8();
        task9();
    }

    //Min element.
    public void task1() {
        try {
            System.out.print("task1 \nInput number to search min: ");

            System.out.println();
            int num = in.nextInt();
            String strNum = Integer.toString(num);
            char[] result = strNum.toCharArray();
            char min = result[0];
            for (char i = 0; i != result.length; i++) {
                if (result[i] < min) {
                    min = result[i];
                }
            }
            System.out.println(min);
        } catch (Exception e) {
            System.out.println("Invalid input!");
        }

    }

    //Max element.
    public void task2() {
        try {
            System.out.print("task2 \nInput number to search max: ");

            System.out.println();
            int num = in.nextInt();
            String strNum = Integer.toString(num);
            char[] result = strNum.toCharArray();
            char max = result[0];
            for (char i = 0; i != result.length; i++) {
                if (result[i] > max) {
                    max = result[i];
                }
            }
            System.out.println(max);
        } catch (Exception e) {
            System.out.println("Invalid input!");
        }

    }

    //Index min element.
    public void task3() {
        try {
            System.out.print("task4 \nInput number to search index min: ");

            System.out.println();
            int num = in.nextInt();
            String strNum = Integer.toString(num);
            char[] result = strNum.toCharArray();
            char min = result[0];
            for (char i = 0; i != result.length; i++) {
                if (result[i] < min) {
                    min = result[i];
                }
            }
            System.out.println("Index min: " + strNum.indexOf(min));
        } catch (Exception e) {
            System.out.println("Invalid input!");
        }
    }

    //Index max element.
    public void task4() {
        try {
            System.out.print("task4 \nInput number to search index max: ");

            System.out.println();
            int num = in.nextInt();
            String strNum = Integer.toString(num);
            char[] result = strNum.toCharArray();
            char max = result[0];
            for (char i = 0; i != result.length; i++) {
                if (result[i] > max) {
                    max = result[i];
                }
            }
            System.out.println("Index max: " + strNum.indexOf(max));
        } catch (Exception e) {
            System.out.println("Invalid input!");
        }
    }

    //Odd amount.
    public void task5() {
        System.out.print("task5 \nOdd amount: ");
        int num = in.nextInt();
        String strNum = Integer.toString(num);
        char[] result = strNum.toCharArray();
        for (int i = 0; i < result.length; i++) {
            if ((result[i] % 2) == 0) {
            } else {
                System.out.println(strNum.indexOf(result[i]));
            }
        }
    }

    //Reverse array.
    public void task6() {
        try {
            System.out.println("task6");

            System.out.print("Input number to reverse: ");
            int num = in.nextInt();
            String changeNum = Integer.toString(num);
            String reverse = new StringBuffer(changeNum).reverse().toString();
            System.out.println(reverse);
        } catch (Exception e) {
            System.out.println("Invalid input!");
        }
    }

    //Count the number of odd array elements.
    public void task7() {
        System.out.println("task7 \nOdd element array: ");
        int num = in.nextInt();
        String strNum = Integer.toString(num);
        char[] result = strNum.toCharArray();
        for (int i = 0; i < result.length; i++) {
            if ((result[i] % 2) == 0) {
            } else {
                System.out.println(result[i]);
            }
        }
    }

    //Swap the first and second half of the array.
    public void task8() {
        System.out.println("task8");
        int num = in.nextInt();
        String strNum = Integer.toString(num);
        char[] result = strNum.toCharArray();
        char[] result1 = Arrays.copyOfRange(result, 0, result.length / 2);
        char[] result2 = Arrays.copyOfRange(result, result.length / 2, result.length);
        System.out.print(result2);
        System.out.println(result1);
    }

    //Sort array.
    public void task9() {
        System.out.println("Task 9");
        int num = in.nextInt();
        String strNum = Integer.toString(num);
        char[] result = strNum.toCharArray();

        //Сортировка массива Bubble
        int j;
        boolean flag = true;
        char temp;
        while (flag) {
            flag = false;
            for (j = 0; j < result.length - 1; j++) {
                if (result[j] > result[j + 1]) {
                    temp = result[j];
                    result[j] = result[j + 1];
                    result[j + 1] = temp;
                    flag = true;
                }
            }
        }
        System.out.println("Bubble sort: " + Arrays.toString(result));

        //Сортировка массива select
        for (int i = 0; i < result.length; i++) {
            int min = result[i];
            int min_i = i;
            for (int x = i + 1; x < result.length; x++) {
                if (result[x] < min) {
                    min = result[x];
                    min_i = x;
                }
            }
            if (i != min_i) {
                char tmp = result[i];
                result[i] = result[min_i];
                result[min_i] = tmp;
            }
        }
        System.out.println("Select sort: " + Arrays.toString(result));

        //Сортировка массива Insert
        for (int i = 1; i < result.length; i++) {
            char currElem = result[i];
            int prevKey = i - 1;
            while (prevKey >= 0 && result[prevKey] > currElem) {
                result[prevKey + 1] = result[prevKey];
                result[prevKey] = currElem;
                prevKey--;
            }
        }
        System.out.println("Insert sort: " + Arrays.toString(result));
    }
}
