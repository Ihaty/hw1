package com.company;

import java.util.Scanner;

public class Base2 {
    Scanner in = new Scanner(System.in);

    public void start() {
        System.out.println("Циклы");
        task1();
        task2();
        task3();
        task4();
        task6();

    }

    //Find the sum of even numbers and their number in the range from 1 to 99.
    public void task1() {
        try {
            System.out.println("task1");
            int count = 0;
            for (int i = 0; i < 100; i++) {
                if ((i % 2) == 0) {
                    System.out.println(i);
                    count++;
                }
            }
            System.out.printf("Amount: %d\n", count);
        } catch (Exception e) {
            System.out.println("Invalid input");
        }

    }

    //Prime number Check.
    public void task2() {
        System.out.println("task2");
        int num = in.nextInt();
        if (num > 1) {
            for (int i = 2; i < num; i++) {
                if (num % i == 0) {
                    System.out.println(num + " Not prime number");
                    return;
                } else {
                    System.out.println(num + " Prime number");
                }
            }
        }
    }

    //Find the root of a natural number.
    public void task3() {
        int num = in.nextInt();
        int result = 0;
        int min = 0;
        int max = num;
        while (min <= max) {
            int mid = (min + max) / 2;
            if ((mid * mid) <= num) {
                min = mid + 1;
                result = mid;
            }
            else if ((mid * mid) > num) {
                max = mid - 1;
            }
        }
        System.out.println(result);
    }

    //Calculate the factorial of a number.
    public void task4() {
        System.out.println("task4");
        int num = in.nextInt();
        int result = num;
        for (int i = 1; (num - i) >= 1; i++) {
            result = result * (num - i);
        }
        System.out.println("Factorial number " + num + ": " + result);
    }

    //Calculate the sum of the digits of a given number.
    public void task5() {
        System.out.println("task5");
        int num = in.nextInt();
        int number = num;
        int result = 0;
        if (number < 0) {
            number = -number;
        }
        while (number > 0) {
            result += number % 10;
            number /= 10;
        }
        System.out.println("Sum numb " + num + ": " + result);
    }

    //Mirror number.
    public void task6() {
        try {
            System.out.println("task5");

            System.out.print("Input number to reverse: ");
            int num = in.nextInt();
            String changeNum = Integer.toString(num);
            String reverse = new StringBuffer(changeNum).reverse().toString();
            System.out.println(reverse);
        } catch (Exception e) {
            System.out.println("Invalid input!");
        }
    }
}
