package com.company;

public class HM1 {

    public static void main(String[] args) {
        Base1 base1 = new Base1();
        Base2 base2 = new Base2();
        Base3 base3 = new Base3();
        Base4 base4 = new Base4();

        //Условные операторы
        base1.start();

        //Циклы
        base2.start();

        //Одномерные массивы
        base3.start();

        //Функции "task - 2 не решено"
        base4.start();
    }
}
