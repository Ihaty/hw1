package com.company;

import java.util.Scanner;

public class Base1 {
    Scanner in = new Scanner(System.in);

    public void start() {
        System.out.println("Условные операторы");
        task1();
        task2();
        task3();
        task4();
        task5();
    }

    //Checking even numbers.
    public void task1() {
        try {
            System.out.println("task1");

            System.out.print("Input a: ");
            int a = in.nextInt();

            System.out.print("Input b: ");
            int b = in.nextInt();

            System.out.println(((a % 2) == 0) ? (a * b) : (a + b));
        } catch (Exception e) {
            System.out.println("Invalid input!");
        }
    }

    //Which x y axis is the point located in?
    void task2() {
        System.out.println("task2");

        System.out.print("Input x: ");
        while (!in.hasNextInt()) {
            System.out.print("This is not a number. \nx: ");
            in.next();
        }
        int x = in.nextInt();


        System.out.print("Input y: ");
        while (!in.hasNextInt()) {
            System.out.print("This is not a number. \ny: ");
            in.next();
        }
        int y = in.nextInt();

        String result = "";
        if (x == 0 || y == 0) {
            System.out.println("Point on axis");
        } else if (x > 0) {
            result = y > 0 ? "I" : "II";
        } else {
            result = y > 0 ? "II" : "III";
        }
        System.out.println("Answer: " + result);
    }

    //Output positive numbers.
    public void task3() {
        try {
            System.out.println("task3");

            int[] arr = new int[3];

            System.out.print("Input num 1: ");
            arr[0] = in.nextInt();

            System.out.print("Input num 2: ");
            arr[1] = in.nextInt();

            System.out.print("Input num 3: ");
            arr[2] = in.nextInt();

            for (int i = 0; i < 3; i++) {
                if (arr[i] > 0) {
                    System.out.println(arr[i]);
                }
            }
        } catch (Exception e) {
            System.out.println("Invalid input!");
        }

    }

    //(max(а*b*с, а+b+с))+3.
    public void task4() {
        try {
            System.out.println("task4");

            int[] arr = new int[3];
            arr[0] = in.nextInt();
            arr[1] = in.nextInt();
            arr[2] = in.nextInt();

            int res1 = arr[0] * arr[1] * arr[2];
            int res2 = arr[0] + arr[1] + arr[2];

            int max = Math.max(res1, res2);
            System.out.println("Output: " + (max + 3));
        } catch (Exception e) {
            System.out.println("Invalid input.");
        }
    }

    //Rating student.
    public void task5() {
        try {
            System.out.println("task5");

            System.out.print("Input rating: ");
            int ratingStudent = in.nextInt();

            if (ratingStudent > 0 && ratingStudent <= 19) {
                System.out.println("F");
            } else if (ratingStudent >= 20 && ratingStudent <= 39) {
                System.out.println("E");
            } else if (ratingStudent >= 40 && ratingStudent <= 59) {
                System.out.println("D");
            } else if (ratingStudent >= 60 && ratingStudent <= 74) {
                System.out.println("C");
            } else if (ratingStudent >= 75 && ratingStudent <= 89) {
                System.out.println("B");
            } else if (ratingStudent >= 90 && ratingStudent <= 100) {
                System.out.println("A");
            }
        } catch (Exception e) {
            System.out.println("Invalid input!");
        }

    }
}
